<?php

namespace App\Http\Controllers;

use App\Models\Order;


use Illuminate\Http\Request;


class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     */

    public function index()
    {
        return inertia(
            'Orders/ShowOrders',
            [
                'orders' => Order::all()
            ]
        );

    }
    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return inertia ('Orders/CreateOrder');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {

        $request->validate([
        'order_content' => 'required|string|min:0| max:500'
        ]);

        $order = new Order();
        $order->order_content = $request->order_content;
        $order->order_status = false;
        $order->save();

        return redirect()->route('orders.index')->with('message', 'Order created successfully');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Order $order)
    {
        return inertia ('Orders/EditOrder',
        [
            'order' => $order
        ]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Order $order)
    {
        $request->validate([
            'order_content' => 'required|string|min:0| max:500'
        ]);

        $order->order_content = $request->order_content;
        $order->order_status = false;
        $order->save();

        return redirect()->route('orders.index')->with('message', 'Order updated');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Order $order)
    {
        $order->delete();
        return redirect()->route('orders.index')->with('message', 'Order deleted');
    }
}
