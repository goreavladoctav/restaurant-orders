const Ziggy = {"url":"http:\/\/localhost","port":null,"defaults":{},"routes":{"sanctum.csrf-cookie":{"uri":"sanctum\/csrf-cookie","methods":["GET","HEAD"]},"ignition.healthCheck":{"uri":"_ignition\/health-check","methods":["GET","HEAD"]},"ignition.executeSolution":{"uri":"_ignition\/execute-solution","methods":["POST"]},"ignition.updateConfig":{"uri":"_ignition\/update-config","methods":["POST"]},"orders.index":{"uri":"orders","methods":["GET","HEAD"]},"orders.create":{"uri":"orders\/create","methods":["GET","HEAD"]},"orders.store":{"uri":"orders","methods":["POST"]},"orders.show":{"uri":"orders\/{order}","methods":["GET","HEAD"]},"orders.edit":{"uri":"orders\/{order}\/edit","methods":["GET","HEAD"],"bindings":{"order":"id"}},"orders.update":{"uri":"orders\/{order}","methods":["PUT","PATCH"],"bindings":{"order":"id"}},"orders.destroy":{"uri":"orders\/{order}","methods":["DELETE"],"bindings":{"order":"id"}}}};

if (typeof window !== 'undefined' && typeof window.Ziggy !== 'undefined') {
    Object.assign(Ziggy.routes, window.Ziggy.routes);
}

export { Ziggy };
